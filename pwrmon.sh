######################################################################
# @author      : oli (oli@oli-HP)
# @file        : pwrmon
# @created     : Saturday Nov 12, 2022 00:46:36 GMT
#
# @description : 
######################################################################
gpu=$(nvidia-smi stats -d pwrDraw -c 1 | awk '{print $NF}')
cpu=$(sensors | grep power | awk '{print $2}')
total=$(echo ${cpu} ${gpu} | awk '{print $1 + $2}')
echo CPU ${cpu}w, GPU ${gpu}w, TOTAL ${total}W


